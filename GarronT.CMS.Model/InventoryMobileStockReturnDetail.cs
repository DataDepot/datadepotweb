//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class InventoryMobileStockReturnDetail
    {
        public long Id { get; set; }
        public long FKInventoryMobileStockReturnId { get; set; }
        public Nullable<long> FKInventorySourceMasterId { get; set; }
        public Nullable<long> ReturnnedToSubId { get; set; }
        public Nullable<long> FKProductId { get; set; }
        public string ProductName { get; set; }
        public int IsNewInventory { get; set; }
        public bool IsSerialNumber { get; set; }
        public string SerialNumber { get; set; }
        public int Quantity { get; set; }
        public Nullable<long> FKInventoryStatusMasterId { get; set; }
        public bool AddedInStock { get; set; }
        public bool AddedInOutwardDetails { get; set; }
        public int Active { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual InventorySourceMaster InventorySourceMaster { get; set; }
        public virtual InventoryStatusMaster InventoryStatusMaster { get; set; }
        public virtual ProductMaster ProductMaster { get; set; }
    }
}
