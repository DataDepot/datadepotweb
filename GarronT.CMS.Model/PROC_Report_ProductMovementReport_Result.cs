//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    
    public partial class PROC_Report_ProductMovementReport_Result
    {
        public string SerialNumber { get; set; }
        public string StockStatus { get; set; }
        public string Location { get; set; }
        public Nullable<System.DateTime> StockMovementDate { get; set; }
        public string UserName { get; set; }
        public string LatLong { get; set; }
        public string Account { get; set; }
        public string Street { get; set; }
        public string ProjectName { get; set; }
    }
}
