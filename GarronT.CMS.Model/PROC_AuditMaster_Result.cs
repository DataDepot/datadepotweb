//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    
    public partial class PROC_AuditMaster_Result
    {
        public long Id { get; set; }
        public string ProjectName { get; set; }
        public string Utilitytype { get; set; }
        public string ClientName { get; set; }
        public System.DateTime AuditStartDate { get; set; }
        public System.DateTime AuditEndDate { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public Nullable<int> Active { get; set; }
    }
}
