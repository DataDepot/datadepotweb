//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    
    public partial class PROC_ReportPDF_Audit_GetReportPDFFields_Result
    {
        public Nullable<long> IDS { get; set; }
        public string sectionName { get; set; }
        public Nullable<long> FieldId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public Nullable<bool> CaptureGeolocation { get; set; }
        public Nullable<bool> CaptureTimestamp { get; set; }
        public string FieldLatitude { get; set; }
        public string FieldLongitude { get; set; }
        public int SortOrderNumber { get; set; }
        public Nullable<int> FieldOrder { get; set; }
        public Nullable<System.DateTime> CapturedDateTime { get; set; }
        public string strFilePath { get; set; }
    }
}
