//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblProjectFieldDataAttachment
    {
        public long ID { get; set; }
        public bool IsSkipped { get; set; }
        public Nullable<long> ProjectFieldDataId { get; set; }
        public long tblProjectFieldDataMaster_ID { get; set; }
        public int IsAudioVideoImage { get; set; }
        public string strFilePath { get; set; }
        public Nullable<bool> isServiceData { get; set; }
        public Nullable<long> tblService_Id { get; set; }
        public Nullable<long> TblServicePicture_ID { get; set; }
        public int Active { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
