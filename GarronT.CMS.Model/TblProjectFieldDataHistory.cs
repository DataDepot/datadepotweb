//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblProjectFieldDataHistory
    {
        public long ID { get; set; }
        public long FormId { get; set; }
        public long tblUploadedData_Id { get; set; }
        public Nullable<long> FK_UploadedExcelData_Id { get; set; }
        public long InstallerId { get; set; }
        public int Active { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<bool> IsSkipped { get; set; }
        public string SkippedReason { get; set; }
        public Nullable<long> SkipId { get; set; }
        public string SkipComment { get; set; }
        public Nullable<System.DateTime> SkippedDatetime { get; set; }
        public Nullable<System.DateTime> visitdatetime { get; set; }
        public string JsonDataVal { get; set; }
        public Nullable<long> InstallerMapId { get; set; }
        public Nullable<bool> IsReAssign { get; set; }
    }
}
