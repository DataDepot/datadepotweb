//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    
    public partial class PROC_Report_InventryStockReport_Summary_Result
    {
        public long AllocatedTo { get; set; }
        public long UserID { get; set; }
        public string InstallerName { get; set; }
        public Nullable<long> CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> ScannedInventry { get; set; }
        public Nullable<int> InstalledInventry { get; set; }
        public Nullable<int> TransferredInventry { get; set; }
        public Nullable<int> ReturnedInventry { get; set; }
        public Nullable<int> InventryInHand { get; set; }
        public long FKProductId { get; set; }
        public string ProductName { get; set; }
    }
}
