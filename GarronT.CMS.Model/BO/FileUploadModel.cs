﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarronT.CMS.Model.BO
{
    public class FileUploadModel
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
