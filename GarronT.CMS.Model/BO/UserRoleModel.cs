﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarronT.CMS.Model.BO
{
    public class UserRoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
