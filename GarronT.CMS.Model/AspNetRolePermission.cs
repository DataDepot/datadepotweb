//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class AspNetRolePermission
    {
        public long ID { get; set; }
        public string Role_Id { get; set; }
        public long Permission_Id { get; set; }
        public bool AddOperation { get; set; }
        public bool EditOperation { get; set; }
        public bool DeleteOperation { get; set; }
        public bool ViewOperation { get; set; }
    
        public virtual AspNetPermission AspNetPermission { get; set; }
        public virtual AspNetRole AspNetRole { get; set; }
    }
}
