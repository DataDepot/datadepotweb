//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class InwardHeader
    {
        public InwardHeader()
        {
            this.InwardDetails = new HashSet<InwardDetail>();
            this.InwardDetails_RejectedData = new HashSet<InwardDetails_RejectedData>();
        }
    
        public long Id { get; set; }
        public int UploadType { get; set; }
        public Nullable<long> FKProjectId { get; set; }
        public Nullable<long> FKWarehouseId { get; set; }
        public string ExcelFilePath { get; set; }
        public Nullable<System.DateTime> DeviceDateTime { get; set; }
        public string GPSLocation { get; set; }
        public string DeviceIP { get; set; }
        public string JsonRequestData { get; set; }
        public int Active { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual ICollection<InwardDetail> InwardDetails { get; set; }
        public virtual ICollection<InwardDetails_RejectedData> InwardDetails_RejectedData { get; set; }
        public virtual WarehouseMaster WarehouseMaster { get; set; }
    }
}
