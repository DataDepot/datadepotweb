//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblVersionMaster
    {
        public int VersionId { get; set; }
        public string VersionNumber { get; set; }
        public string VersionDescription { get; set; }
        public int Active { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedOn { get; set; }
        public Nullable<System.DateTime> ModifiedBy { get; set; }
    }
}
