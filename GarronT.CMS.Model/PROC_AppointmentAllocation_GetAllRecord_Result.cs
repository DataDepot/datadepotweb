//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GarronT.CMS.Model
{
    using System;
    
    public partial class PROC_AppointmentAllocation_GetAllRecord_Result
    {
        public string AccountNumber { get; set; }
        public string Cycle { get; set; }
        public string Route { get; set; }
        public string Street { get; set; }
        public Nullable<System.DateTime> FirstPreferedDate { get; set; }
        public string FirstTime { get; set; }
        public long AppointmentID { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public Nullable<long> FKProjectId { get; set; }
        public string UtilityType { get; set; }
        public long InstallerId { get; set; }
        public string InsatllerName { get; set; }
        public string ProjectCityName { get; set; }
        public Nullable<int> PassedDateFlag { get; set; }
        public Nullable<int> AppointmentFlag { get; set; }
    }
}
